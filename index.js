// import lib express
const express = require("express");

// init app express
const app = express();

// declare port
const port = 8000;

// declare to use request body json
app.use(express.json());

// declare GET API trả ra simple message
app.get("/", (request, response) => {
    let today = new Date();

    let string = `Xin chào, hôm nay là ngày ${today.getDate()}-${today.getMonth() + 1}-${today.getFullYear()} .`

    response.json({
        message: string
    }) 
})
var array = ["a", "b", "c"]

// method GET
app.get("/methods", (request, response) => {
    response.json(array);
})
// method GET with param
app.get("/methods/:paramindex", (request, response) => {
    const index = request.params.paramindex;
    response.json(array[index]);
})
// method GET with request query
app.get("/methods-query/", (request, response) => {
    const query = request.query;

    const param = query.param;

    const find_param = array.findIndex(element => element == param);

    response.json(find_param);
})

//method POST
app.post("/methods", (request, response) => {
    array = [...array, "d"];
    response.json(array)
})
//method POST body json
app.post("/methods-body-json", (request, response) => {
    const body = request.body;
    response.json(body)
})
//method PUT
app.put("/methods", (request, response) => {
    array[0] = "z";
    response.json(array);
})
//method DELETE
app.delete("/methods", (request, response) => {
    let [first, ...slice_array] = array;
    array = slice_array;
    response.json(array);
})

// run app on declared port
app.listen(port, () => {
    console.log(`App running on port ${port}`);
})